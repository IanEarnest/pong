To-do list
==========

&#x2611; Paddles

- &#x2611; Player movement
	
- &#x2611; A.I. movement
	
- &#x2611; Movement restriction
	
- &#x2611; Ball collision
	
- &#x2611; Scoring
	
&#9744; Levels

- &#x2611; Main
	
- &#9744; Game
	
- &#x2611; Scores
	
&#x2611; Publish web player

&#x2611; Local high scores

&#9744; Global high scores

&#9744; Local multiplayer

&#9744; Global multiplayer

&#9744; Additional game modes

&#9744; Adjust gameplay/speeds

&#9744; Design/colour system



Map

&#x2611; = finished

&#9744; = in progress
